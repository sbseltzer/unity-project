3rd Party Folder
================

This folder is intended for assets that were not made by you or your team.

This will probably have a lot of Asset Store content, and maybe some content from external repositories.

NOTE: Not all external assets can go in here. For instance, the Standard Assets folders MUST be in the base Assets directory. Some Editor scripts depend on being loaded in a particular order, which may mean having an Editor folder in the base Assets directory. Some assets also hard-code their configuration file storage location, so moving them here may cause them to behave strangely or recreate their content in the hard-coded location. In general, if it puts content in a "Special Folder" in the base Assets directory, it's probably safest to leave it alone.
