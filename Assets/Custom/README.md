Custom Folder
=============

This folder is for custom (in-house) asset files.

Some projects will have lots of custom asset types made specifically for that project. These are often in the form of Scriptable Objects that are used to configure other Scriptable Objects and Components.

The purpose of this folder is very similar to the General folder in that it stores arbitrary asset files, but the separation exists to keep Custom scripted content from getting mangled with content that's used by 3rd Party and stock Unity assets.
