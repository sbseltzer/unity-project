Sandbox Folder
==============

All prototyping should take place here. If you work on a team, it may be a good idea to make folder for each teammate to work in.

To keep dependencies decoupled, never allow anything outside this folder to reference anything inside this folder. Once a prototype has become a full-fledged feature outside of this folder, delete the prototype content or move it to a more appropriate folder. 

If prototypes are something you're interested in keeping around for posterity, consider using a version control system (SVN, Git, etc.) so it may be refered to later.
