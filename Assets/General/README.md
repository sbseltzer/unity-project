General Folder
==============

In short, anything can go in here. It namely exists to prevent asset files from appearing in the base Assets folder.

There may be some assets in your project that don't have enough of the same kind to deserve their own folder. Maybe it's an asset that doesn't have a clear place it belongs. 

This folder is for those sorts of assets until a better place is decided upon (if ever).
