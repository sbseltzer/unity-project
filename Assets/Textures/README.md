Textures Folder
================

Keep your textures here.

NOTE: If other assets include textures when they're imported (i.e. models), resist the urge to move any of it so you can keep the imported content together. This is so reorganizing and/or deleting that content doesn't leave redundant or useless files around cluttering your project.
