Models Folder
================

Keep your models here.

NOTE: Models can import Materials and Textures. Resist the urge to move those other assets elsewhere even when there is a folder for them. Keeping group imports together is good practice so that it is dealt with as a single unit of content.
