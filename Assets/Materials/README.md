Materials Folder
================

Keep your materials here. You're welcome to interpret/organize this as you wish, since there are Material and PhysicMaterial asset types.

NOTE: If other assets include materials when they're imported (i.e. models), resist the urge to move any of it here so you can keep the imported content together. This is so reorganizing and/or deleting that content doesn't leave redundant or useless files around cluttering your project.
