Assets Folder
=============

This is a structured Assets folder that's been designed to keep project content organized in a sane fashion.

It is loosely based on the structure described by [this Best Practices article](http://blog.theknightsofunity.com/7-ways-keep-unity-project-organized/).

You are encouraged to augment this structure to suit your needs or personal aesthetic taste (unless you already feel this is perfect).

There may even be more folders here than you'll ever need. That's great! Delete what you don't need. You can always add it back later.

Guidelines
----------

* Do not create assets in this base Assets directory unless absolutely necessary. For instance, some 3rd Party assets require some of their content to be in specific locations, which should be relatively rare. If you have some asset that doesn't seem to belong anywhere, the General or Custom folders may be good candidates. 

* There are some exceptions to storing things in their alloted folder. For instance, if an imported asset imports more than one kind of asset (i.e. models that include materials and/or textures, temporary prototype content, 3rd party assets, etc.), you should keep that content together in the folder that makes most sense to you.

