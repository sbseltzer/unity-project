Scripts Folder
==============

Keep your scripts in here.

The Scripts folder really deserves its own "Best Practice" guide for organizing its content, but it's ultimately going to come down to your individual needs.

Many projects have an Editor folder in this directory. Editor folders can technically go anywhere and still work properly, so it's up to you where they/it go/goes if you even require any Editor folders.
